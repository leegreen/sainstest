<?php
/**
 * Created by PhpStorm.
 * User: leegreen
 * Date: 16/04/15
 * Time: 22:38
 */

/* global php ini setups */
ini_set('max_execution_time','3000');
ini_set('memory_limit','24M'); /* increase if needed, set low then work upwards */
error_reporting(E_ERROR); /* dont show warnings, notices etc. only full on errors */

require 'classes/PageScrapper.php';
require 'simple_html_dom.php';

/* was going to use this but seemed little over kill for one class
function __autoload($class_name) {
    require_once $_SERVER['DOCUMENT_ROOT'].'/../classes/'.$class_name . '.php';
}
*/