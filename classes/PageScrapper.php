<?php
/**
 * Created by PhpStorm.
 * User: leegreen
 * Date: 16/04/15
 * Time: 15:41
 */

class PageScrapper {
    private $pageUrl;
    private $products;

    function __construct($url=false) {
        $this->pageUrl = $url;
        $this->products = array("total"=>0);
    }

    private function getLinkContent($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); /* allow redirects */
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); /* return into a variable */
        curl_setopt($ch, CURLOPT_TIMEOUT, 10); /* times out after 10s */
        curl_setopt($ch, CURLOPT_HEADER, 1);

        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:11.0) Gecko/20100101 Firefox/11.0"); /* set useragent so site thinks we are a real user */
        $cookie_file = "/../tmp/cookie1.txt"; /* create cookie file to allow the product content to be shown. Not sure what the cookie is tracking about us */
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
        return curl_exec($ch); /* run the whole process - return response */
    }

    public function mockgetRemoteSize($content) {
        return $this->getRemoteSize($content);
    }

    private function getRemoteSize($content) {
        $tmpFileName = '/../tmp/tmp.html';
        file_put_contents($tmpFileName,$content); /* save file, to got size on disk */
        $fileSize = filesize($tmpFileName); /* get size */
        unlink($tmpFileName); /* clean up */
        return number_format($fileSize/1024,2); /* return in kb */
    }

    public function addProduct($product) {
        $this->products['results'][] = $product;
        $this->products['total'] = $this->products['total'] + $product['unit_price']; /* increment the unit price */
    }

    public function getResultsJson() {
        return json_encode($this->products);
    }

    public function startScrap() {
        $rootPageHtml = $this->getLinkContent($this->pageUrl);/* get the root page content */
        $html = new simple_html_dom();/* create parser object */
        $html->load($rootPageHtml);/* Parse the HTML */
        foreach($html->find('div[class=product]') as $divOuter) {
            $product = array();
            $linkObj = $divOuter->find('div', 0)->find('div', 0)->find('div', 0)->find('h3', 0)->find('a', 0); /* get the link object. it contents the link and the title */
            $product['unit_price'] = preg_replace("/[^0-9.]/", "", $divOuter->find('div', 0)->find('div', 2)->find('div', 0)->find('p', 0)->plaintext); /* get the unit price / strip anything that isnt numeric or . */
            $product['title'] = trim($linkObj->plaintext); /* get the product name */
            $productDetail = $this->getLinkContent($linkObj->href); /* get content of the linked detail page */
            $htmlDetailPage = new simple_html_dom();
            $htmlDetailPage->load($productDetail); /* create new parse object for the detail page */
            $product['description'] = trim(strip_tags($htmlDetailPage->find('div[class=productText]',0)->plaintext)); /* get only the description content, strip html, and trim white space before and after */
            $product['size'] = $this->getRemoteSize($productDetail).'kb';
            $this->addProduct($product); /* add this product to the array */
        }
    }
} 