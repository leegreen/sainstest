<?php
require_once '../config.php';
class genericTests extends PHPUnit_Framework_TestCase {
    public function setUp(){ }
    public function tearDown(){ }

    public function testJsonStructureSingle() {
        $obj = new PageScrapper();
        /* dummy json to check structure */
        $product = array('unit_price'=>5,'title'=>'test content','description'=>'test description','size'=>'10kb');
        $obj->addProduct($product);
        $json = $obj->getResultsJson();
        $this->assertEquals('{"total":5,"results":[{"unit_price":5,"title":"test content","description":"test description","size":"10kb"}]}',$json);
    }

    public function testJsonStructureMultiple() {
        $obj = new PageScrapper();
        /* dummy json to check structure */
        $product = array('unit_price'=>5,'title'=>'test content','description'=>'test description','size'=>'10kb');
        $obj->addProduct($product);
        $product = array('unit_price'=>10,'title'=>'test content 2','description'=>'test description 2','size'=>'1kb');
        $obj->addProduct($product);
        $json = $obj->getResultsJson();
        $this->assertEquals('{"total":15,"results":[{"unit_price":5,"title":"test content","description":"test description","size":"10kb"},{"unit_price":10,"title":"test content 2","description":"test description 2","size":"1kb"}]}',$json);
    }

    public function testCheckFileSize() {
        $obj = new PageScrapper();
        /* check file can be saved, and read */
        $file = 'ddsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdds';
        $fileSize = $obj->mockgetRemoteSize($file);
        $this->assertEquals('0.08',$fileSize);
    }

    public function testCheckFileCleanup() {
        $obj = new PageScrapper();
        /* check file can be saved, and read */
        $file = 'dummycontent';
        $obj->mockgetRemoteSize($file);
        $this->assertFalse(file_exists('/../tmp/tmp.html'));
    }
}